<?php
require_once 'lib/common.php';
session_start();
noMagicQuotes();

if (!empty($_REQUEST['login']) && !empty($_REQUEST['password'])) {
	$db = initDatabase();
	$sql = "SELECT * FROM user "
		."WHERE login='".$_POST['login']."' AND password='".$_POST['password']."'";
	$users = $db->query($sql)->fetchAll(PDO::FETCH_OBJ);
	if (!empty($users)) {
		$_SESSION['user'] = $users[0];
		// redirection (syntaxe incorrecte, il faut normalement une URL complète)
		header('Location: article_list.php');
		exit();
	}
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
        <title>Sécurité PHP - login</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<h1>Authentification et injection SQL</h1>
<form action="" method="POST">
<fieldset>
        Login : <input name="login" type="text" value="<?php if (isset($_REQUEST['login'])) { echo $_REQUEST['login']; } ?>" /> <br />
        Mot de passe : <input name="password" type="text" value="" /> <br />
        <button type="submit" name="ok" value="1">S'authentifier</button>
</fieldset>
</form>

<h1>Code source de cette page</h1>
<div style="border-left: 3px solid red; padding-left: 1em;">
<?php highlight_file(__FILE__); ?>
</div>

</body>
</html>
