<?php
require_once 'lib/common.php';
session_start();
noMagicQuotes();

$db = initDatabase();

if (empty($_REQUEST['id_article'])) {
	// redirection (syntaxe incorrecte, il faut normalement une URL complète)
	header('Location: article_list.php');
	exit();
}
if (!empty($_GET['title']) && !empty($_GET['content'])) {
	$title = $_GET['title'];
	$content = $_GET['content'];
	if (empty($_GET['id_comment'])) { // nouveau ou modif ?
		$sql = "INSERT INTO comment (id_article, title, content, id_user) "
			."VALUES (".$_GET['id_article'].", '$title', '$content', ".$_SESSION['user']->id.")";
	} else {
		$sql = "UPDATE comment SET title='$title', content='$content', id_user=". $_SESSION['user']->id
			." WHERE id = " . $_GET['id_comment'];
	}
	if ($db->query($sql)) {
		// redirection (syntaxe incorrecte, il faut normalement une URL complète)
		header('Location: article_view.php?id=' . $_GET['id_article']);
		exit();
	} else {
		die("Erreur SQLite (permission d'écriture sur le fichier et son répertoire ?) : $sql");
	}
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
        <title>Sécurité PHP - commentaire</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<h1>Ajouter/modifier un commentaire</h1>
<form action="" method="get">
<fieldset>
<?php if (!empty($_REQUEST['id_comment'])) {
	echo '<input name="id_comment" type="hidden" value="' . $_REQUEST['id_comment'] ."\" />\n";
} ?>
        <input name="id_article" type="hidden" value="<?php echo $_REQUEST['id_article']; ?>" /> <br />
        Titre : <input name="title" type="text" value="" size="60" /> <br />
        Texte : <textarea name="content" cols="60" rows="6"></textarea> <br />
        <button type="submit" name="ok" value="1">Ajouter ce commentaire</button>
</fieldset>
</form>

<h1>Code source de cette page</h1>
<div style="border-left: 3px solid red; padding-left: 1em;">
<?php highlight_file(__FILE__); ?>
</div>

</body>
</html>
