<?php
require_once 'lib/common.php';
session_start();
noMagicQuotes();

if (empty($_GET['id'])) {
	// redirection (syntaxe incorrecte, il faut normalement une URL complète)
	header('Location: article_list.php');
	exit();
}

$db = initDatabase();
$articles = $db->query("SELECT * FROM article WHERE id=" . $_GET['id'])
               ->fetchAll(PDO::FETCH_OBJ);
$article = $articles[0];
$article->comments = $db->query("SELECT c.*, u.login, u.url "
								. "FROM comment c JOIN user u ON c.id_user=u.id"
								. " WHERE id_article=" . $_GET['id'])
                        ->fetchAll(PDO::FETCH_OBJ);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
        <title>Sécurité PHP - article</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<h1>Article</h1>

<?php

echo '<div id="article">'
	. '<h2>'. $article->title .'</h2>'
	. '<div id="content">' . $article->content . '</div>';

echo '<h3>Commentaires</h3>';
if (empty($article->comments)) {
	echo '<p>Aucun</p>';
} else {
	echo '<dl>';
	foreach ($article->comments as $comment) {
		echo '<dt>'
			. $comment->title
			. (isset($_SESSION['user']->id) && $comment->id_user == $_SESSION['user']->id ?
			   ' <a href="comment_create.php?id_article=' . $comment->id_article
			   . '&amp;id_comment=' . $comment->id . '" title="'. $comment->title
			   . '">Modifier ce commentaire</a>' :
			   '')
			. '</dt>'
			. '<dd>' . $comment->content 
			.  "<div><a href=\"$comment->url\">$comment->login</a></div>"
			. "</dd>\n";
	}
	echo "</dl>\n";
}

echo "</div>\n";

if (empty($_SESSION['user'])) {
	echo '<p>Il faut être identifié pour poster un commentaire.</p>';
} else {
	if ($article->closed) {
		echo "<p>Article fermé, non modifiable.</p>";
	} else {
		echo '<p> <a href="comment_create.php?id_article='. $article->id
			.'">Ajouter un commentaire</a> avec votre compte : ' . $_SESSION['user']->name
			.' </p>';
	}
}
?>

<p> <a href="article_list.php">Retour à la liste des articles</a> </p>

<h1>Code source de cette page</h1>
<div style="border-left: 3px solid red; padding-left: 1em;">
<?php highlight_file(__FILE__); ?>
</div>

</body>
</html>
