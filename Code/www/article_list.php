<?php
require_once 'lib/common.php';
session_start();
noMagicQuotes();

$db = initDatabase();
$articles = $db->query("SELECT * FROM article")->fetchAll(PDO::FETCH_OBJ);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
        <title>Sécurité PHP - liste des articles</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<h1>Liste des articles</h1>

<?php
if (!empty($_SESSION['user'])) {
	echo "<p>Bonjour, " . $_SESSION['user']->name . ".</p>";
}
?>

<ul>
<?php
foreach ($articles as $article) {
	echo '<li><a href="article_view.php?id=' . $article->id .'">'
		. $article->title . "</a></li>\n";
}
?>
</ul>

</body>
</html>
