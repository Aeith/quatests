<?php
require_once 'lib/common.php';
session_start();
noMagicQuotes();

if (!empty($_POST['name']) && !empty($_POST['login']) && !empty($_POST['password'])) {
	$db = initDatabase();
	$sql = "INSERT INTO user (name, login, password, url) "
		."VALUES ('".$_POST['name']."', '".$_POST['login']."', '".$_POST['password']
		."', '" . $_POST['url'] . "')";
	if ($db->query($sql)) {
		// redirection (syntaxe incorrecte, il faut normalement une URL complète)
		header('Location: user_login.php');
		exit();
	} else {
		die("Erreur SQLite (permission d'écriture sur le fichier et son répertoire ?) : $sql");
	}
}
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
                      "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>
        <title>Sécurité PHP - création de compte</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>

<h1>Création de compte</h1>
<form action="" method="POST">
<fieldset>
        Nom : <input name="name" type="text" value="" /> <br />
        Site perso : <input name="url" type="text" value="" /> <br />
        Login : <input name="login" type="text" value="" /> <br />
        Mot de passe : <input name="password" type="text" value="" /> <br />
        <button type="submit" name="ok" value="1">Créer ce compte</button>
</fieldset>
</form>

<p> <a href="article_list.php">Retour à la liste des articles</a> </p>

<h1>Code source de cette page</h1>
<div style="border-left: 3px solid red; padding-left: 1em;">
<?php highlight_file(__FILE__); ?>
</div>

</body>
</html>
