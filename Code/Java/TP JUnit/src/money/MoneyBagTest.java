package money;

import java.util.HashMap;

import org.junit.Assert;
import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test Class for MoneyBag
 * @author Aeith
 *
 */
class MoneyBagTest {
	
	private static HashMap<String, String> convert;
	
	@BeforeAll
	static void initConvert() {
		convert = new HashMap<String, String>();
		convert.put("EUR", "1.0");
		convert.put("USD", "0.9");
		convert.put("CHF", "1.3");
		convert.put("GBP", "1.1");
	}

	@Test
	void testEmptyConstructor() {
		MoneyBag mb = new MoneyBag();
		
		int expectedAmount = 0;
		String expectedCurrency = "EUR";
		
		HashMap<String, String> expectedConvert = new HashMap<String, String>();
		
		Money moneyActual = mb.money();
		
		Assert.assertEquals(expectedAmount, moneyActual.amount());
		Assert.assertEquals(expectedCurrency, moneyActual.fCurrency());
		Assert.assertEquals(expectedConvert, mb.convert());
	}
	
	@Test
	void testConstructor() {
		Money m = new Money(10, "EUR");
		MoneyBag mb = new MoneyBag(m, convert);

		int expectedAmount = 10;
		String expectedCurrency = "EUR";
		
		Money moneyActual = mb.money();

		Assert.assertEquals(expectedAmount, moneyActual.amount());
		Assert.assertEquals(expectedCurrency, moneyActual.fCurrency());
		Assert.assertEquals(convert, mb.convert());
	}
	
	@Test
	void testInsert() {
		MoneyBag mb = new MoneyBag();
		mb.insert("YEN", "0.5");
		HashMap<String, String> expected = new HashMap<String, String>();
		
		expected.put("YEN", "0.5");
		
		Assert.assertEquals(expected, mb.convert());
	}
	
	@Test
	void testInsertAgain() {
		Money m = new Money();
		MoneyBag mb = new MoneyBag(m, convert);
		
		mb.insert("EUR", "0.5");
		HashMap<String, String> expected = convert;
		
		Assert.assertEquals(expected, mb.convert());
	}
	
	@Test
	void testAdd() {
		MoneyBag mb = new MoneyBag(new Money(), convert);
		Money m = new Money(6, "EUR");
		
		int expected = 6;
		int actual = mb.add(m).amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testAddDifferent() {
		MoneyBag mb = new MoneyBag(new Money(), convert);
		Money m = new Money(10, "GBP");
		
		int expected = 11;
		int actual = mb.add(m).amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testAddNegative() {
		MoneyBag mb = new MoneyBag(new Money(), convert);
		Money m = new Money(-10, "EUR");
		
		int expected = 0;
		int actual = mb.add(m).amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testAddDifferentNegative() {
		MoneyBag mb = new MoneyBag(new Money(), convert);
		Money m = new Money(-10, "GBP");
		
		int expected = 0;
		int actual = mb.add(m).amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testAddMoneyBag() {
		Money m = new Money(10, "EUR");
		MoneyBag mb = new MoneyBag(new Money(), convert);
		MoneyBag mb2 = new MoneyBag(m, convert);
		
		int expected = 10;
		int actual = mb.add(mb2).money().amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testAddDifferentMoneyBag() {
		Money m = new Money(10, "GBP");
		MoneyBag mb = new MoneyBag(new Money(), convert);
		MoneyBag mb2 = new MoneyBag(m, convert);
		
		int expected = 11;
		int actual = mb.add(mb2).money().amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testAddNegativeMoneyBag() {
		Money m = new Money(-10, "EUR");
		MoneyBag mb = new MoneyBag(new Money(), convert);
		MoneyBag mb2 = new MoneyBag(m, convert);
		
		int expected = 0;
		int actual = mb.add(mb2).money().amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testAddDifferentNegativeMoneyBag() {
		Money m = new Money(-10, "GBP");
		MoneyBag mb = new MoneyBag(new Money(), convert);
		MoneyBag mb2 = new MoneyBag(m, convert);
		
		int expected = 0;
		int actual = mb.add(mb2).money().amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testSub() {
		MoneyBag mb = new MoneyBag(new Money(), convert);
		Money m = new Money(6, "EUR");
		
		int expected = -6;
		int actual = mb.sub(m).amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testSubDifferent() {
		MoneyBag mb = new MoneyBag(new Money(), convert);
		Money m = new Money(10, "GBP");
		
		int expected = -11;
		int actual = mb.sub(m).amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testSubNegative() {
		MoneyBag mb = new MoneyBag(new Money(), convert);
		Money m = new Money(10, "EUR");
		
		int expected = 0;
		int actual = mb.sub(m).amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testSubDifferentNegative() {
		MoneyBag mb = new MoneyBag(new Money(), convert);
		Money m = new Money(-10, "GBP");
		
		int expected = 0;
		int actual = mb.sub(m).amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testSubMoneyBag() {
		Money m = new Money(10, "EUR");
		MoneyBag mb = new MoneyBag(new Money(), convert);
		MoneyBag mb2 = new MoneyBag(m, convert);
		
		int expected = -10;
		int actual = mb.sub(mb2).money().amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testSubDifferentMoneyBag() {
		Money m = new Money(10, "GBP");
		MoneyBag mb = new MoneyBag(new Money(), convert);
		MoneyBag mb2 = new MoneyBag(m, convert);
		
		int expected = -11;
		int actual = mb.sub(mb2).money().amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testSubNegativeMoneyBag() {
		Money m = new Money(-10, "EUR");
		MoneyBag mb = new MoneyBag(new Money(), convert);
		MoneyBag mb2 = new MoneyBag(m, convert);
		
		int expected = 0;
		int actual = mb.sub(mb2).money().amount();
		
		Assert.assertEquals(expected, actual);
	}
	
	@Test
	void testSubDifferentNegativeMoneyBag() {
		Money m = new Money(-10, "GBP");
		MoneyBag mb = new MoneyBag(new Money(), convert);
		MoneyBag mb2 = new MoneyBag(m, convert);
		
		int expected = 0;
		int actual = mb.sub(mb2).money().amount();
		
		Assert.assertEquals(expected, actual);
	}
}
