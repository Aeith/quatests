package money;

import java.util.HashMap;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import org.junit.Assert;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

class MoneyTestParameterized {

	// Local
	private MoneyBag moneyBag = new MoneyBag(new Money(10, "EUR"), new HashMap<String, String>());
	
	/**
	 * Test sub with positive and negative values
	 * @param money
	 */
	@ParameterizedTest
	@MethodSource("getMoneys")
	void test(final Money money) {
		final Money m = this.moneyBag.sub(money);
		Assert.assertTrue(m.amount() >= 0);
	}

	/**
	 * Generate 10 Money from -5 to 5 amount
	 * @return
	 */
	static Stream<Money> getMoneys() {
		return IntStream.range(0, 10).mapToObj(i -> new Money(i-5, "EUR"));
	}
}
