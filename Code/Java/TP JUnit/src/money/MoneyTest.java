package money;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

/**
 * Test Class for Money
 * @author Aeith
 *
 */
class MoneyTest {
	
	//@Before
	
	@Test
	void testEmptyConstructor() {
		Money m = new Money();
		int amount = m.amount();
		String currency = m.fCurrency();
		
		Assert.assertEquals(0, amount);
		Assert.assertEquals("EUR", currency);
	}
	
	@Test
	void testConstructor() {
		Money m = new Money(12, "GBP");
		int amount = m.amount();
		String currency = m.fCurrency();
		
		Assert.assertEquals(12, amount);
		Assert.assertEquals("GBP", currency);
	}

	@Test
	void testAddMoneySame() {
		Money m = new Money(10, "EUR");
		Money m2 = new Money(5, "EUR");
		int expected = 15;
		
		int actual = m.add(m2).amount();
		
		Assert.assertEquals(expected, actual);
	}

	@Test
	void testAddMoneyDifferent() {
		Money m = new Money(10, "EUR");
		Money m2 = new Money(5, "GBP");
		int expected = 10;
		
		int actual = m.add(m2).amount();
		
		Assert.assertEquals(expected, actual);
	}

	@Test
	void testAddMoneyNegative() {
		Money m = new Money(10, "EUR");
		Money m2 = new Money(-5, "EUR");
		int expected = 10;
		
		int actual = m.add(m2).amount();
		
		Assert.assertEquals(expected, actual);
	}

	@Test
	void testAddMoneyIntString() {
		Money m = new Money(10, "EUR");
		int expected = 15;
		
		int actual = m.add(5, "EUR").amount();
		
		Assert.assertEquals(expected, actual);
	}

	@Test
	void testAddMoneyIntStringDifferent() {
		Money m = new Money(10, "EUR");
		int expected = 10;
		
		int actual = m.add(5, "GBP").amount();
		
		Assert.assertEquals(expected, actual);
	}

	@Test
	void testAddMoneyIntStringNegative() {
		Money m = new Money(10, "EUR");
		int expected = 5;
		
		int actual = m.add(-5, "EUR").amount();
		
		Assert.assertEquals(expected, actual);
	}
}
