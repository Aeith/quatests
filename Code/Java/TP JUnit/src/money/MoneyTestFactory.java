package money;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DynamicTest;
import org.junit.jupiter.api.TestFactory;

import money.*;

public class MoneyTestFactory {
	
	private HashMap<String, String> convert;
	private MoneyBag moneyBag;
	
	/**
	 * Prepare converter and money bag
	 */
	@BeforeEach
	void init() {
		this.convert = new HashMap<String, String>();
		this.convert.put("EUR", "1.0");
		this.convert.put("USD", "0.9");
		this.convert.put("CHF", "1.3");
		this.convert.put("GBP", "1.1");
		
		this.moneyBag = new MoneyBag(new Money(10, "EUR"), convert);
	}

	/**
	 * Generate tests for different currencies and values
	 * @return
	 */
	@TestFactory
	Stream<DynamicTest> testFactory() {
		List<Money> inputs = new ArrayList<Money>();
		inputs.add(new Money(1, "EUR"));
		inputs.add(new Money(10, "GBP"));
		inputs.add(new Money(-5, "EUR"));
		inputs.add(new Money(0, "EUR"));
		inputs.add(new Money(-10, "USD"));
		inputs.add(new Money(10, "USD"));
		
		List<Integer> results = new ArrayList<Integer>();
		results.add(11);
		results.add(21);
		results.add(10);
		results.add(10);
		results.add(10);
		results.add(19);
		
		return inputs.stream()
				.map(termes -> DynamicTest.dynamicTest("Add : " + termes, () -> {
						int i = inputs.indexOf(termes);
						assertEquals(results.get(i), this.moneyBag.add(termes).amount());
				}));
	}

}
