<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Insert title here</title>
	</head>
	
	<body>
		<form action="${pageContext.request.contextPath}/controleur" method="post">
			<fieldset>
				<legend>Banque</legend>
				
				<p>
					<label for="leftInput">Montant :</label>
					<input type="text" name="leftInput" id="leftInput" />
					<label for="leftDropDownList">Devises :</label>
					<!-- J'aurais p� faire une liste g�n�r�e depuis une liste de devises mais le plus propre aurait �t� de faire un classe devise avec un label, acronyme etc et pas envie -->
					<select name="leftDrowDownList" id="leftDropDownList">
						<option value="EUR">EUR</option>
						<option value="USD">USD</option>
						<option value="CHF">CHF</option>
						<option value="GBP">GBP</option>
					</select>
				</p>
				
				<p>
					<label for="rightInput">Montant :</label>
					<input type="text" name="rightInput" id="rightInput" />
					<label for="rightDropDownList">Devises :</label>
					<select name="rightDropDownList" id="rightDropDownList">
						<option value="EUR">EUR</option>
						<option value="USD">USD</option>
						<option value="CHF">CHF</option>
						<option value="GBP">GBP</option>
					</select>
				</p>
				
				<p>
					<input type="submit" value="Ajouter" onclick="" />
				</p>
				
				<p>
					<label for="resultInput">R�sultat :</label>
					<input type="text" name="resultInput" id="resultInput" value="<%= request.getAttribute("result") %>" />
				</p>
			</fieldset>
		</form>
	</body>
</html>