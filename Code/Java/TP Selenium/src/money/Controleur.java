package money;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Controleur
 */
@WebServlet("/controleur")
public class Controleur extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private MoneyBag mb;
	private HashMap<String, String> converter;
	
    /**
     * Default constructor. 
     */
    public Controleur() {
        converter = new HashMap<String, String>();
        converter.put("EUR", "1.0");
        converter.put("USD", "0.9");
        converter.put("CHF", "1.3");
        converter.put("GBP", "1.1");
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		
		String result = "";
		request.setAttribute("result",result);
		request.getRequestDispatcher("View.jsp").forward(request, response); 
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Retrieve results
		int leftValue = Integer.parseInt(request.getParameter("leftInput"));
		int rightValue = Integer.parseInt(request.getParameter("rightInput"));
		String leftCurrency = request.getParameter("leftDropDownList");
		String rightCurrency = request.getParameter("rightDropDownList");
	
		// Do addition
		mb = new MoneyBag(new Money(leftValue, leftCurrency), converter);
		Money m = mb.add(new Money(rightValue, rightCurrency));
		int result = m.amount();
		
		// Redirect result to JSP
		request.setAttribute("result",result);
		request.getRequestDispatcher("View.jsp").forward(request, response); 
	}

}
