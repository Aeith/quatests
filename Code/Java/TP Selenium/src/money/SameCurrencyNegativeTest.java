package money;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import java.util.*;

public class SameCurrencyNegativeTest {
  private WebDriver driver;
  private Map<String, Object> vars;
  JavascriptExecutor js;
  @Before
  public void setUp() {
    driver = new FirefoxDriver();
    js = (JavascriptExecutor) driver;
    vars = new HashMap<String, Object>();
  }
  @After
  public void tearDown() {
    driver.quit();
  }
  @Test
  public void sameCurrencyNegative() {
    driver.get("http://localhost:8086/TP_Selenium/controleur");
    driver.manage().window().setSize(new Dimension(550, 691));
    driver.findElement(By.id("leftInput")).click();
    driver.findElement(By.id("leftInput")).sendKeys("10");
    driver.findElement(By.id("rightInput")).sendKeys("-5");
    driver.findElement(By.cssSelector("input:nth-child(1)")).click();
  }
}
