package money;

/**
 * Money class
 * @author Aeith
 *
 */
public class Money {
	/**
	 * Fields
	 * @param namounnt int
	 * @param ncurrency String
	 */
	private int fAmount;
	private String fCurrency;

	/**
	 * Getters
	 */
	public int amount() { return this.fAmount; }
	public String fCurrency() { return this.fCurrency; }

	/**
	 * Constructor
	 */
	public Money() {
		this.fAmount = 0;
		this.fCurrency = "EUR";
	}
	/**
	 * Constructor
	 * @param amount int
	 * @param current String
	 */
	public Money(int amount, String current) {
		this.fAmount = amount;
		this.fCurrency = current;
	}
	
	/**
	 * Add m amount with this amount if same currency
	 * @param m Money
	 * @return Money
	 */
	public Money add(Money m) {
		String c = m.fCurrency();
		
		if(c == this.fCurrency() && m.amount() > 0) {
			int sum = m.amount() + amount();
			return new Money(sum, this.fCurrency());
		}
		
		return this;
	}
	
	/**
	 * Add an amount with the specified currency to this amount
	 * @param namount
	 * @param ncurrency
	 * @return
	 */
	public Money add(int namount, String ncurrency) {
		if(ncurrency == this.fCurrency()) {
			int sum = namount + amount();
			return new Money(sum, this.fCurrency());
		}
		
		return this;
	}
}
