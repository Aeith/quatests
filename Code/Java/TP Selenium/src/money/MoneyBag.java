package money;

import java.util.HashMap;

/**
 * MoneyBag class
 * @author Aeith
 *
 */
public class MoneyBag {

	/**
	 * Fields
	 */
	private Money fMoney;
	private HashMap<String, String> fConvert;
	
	/**
	 * Getters
	 */
	public Money money() { return this.fMoney; }
	public HashMap<String, String> convert() { return this.fConvert; }
	
	/**
	 * Constructor
	 */
	public MoneyBag() {
		this.fMoney = new Money();
		this.fConvert = new HashMap<String, String>();
	}
	/**
	 * Constructor
	 * @param money Money
	 * @param convert HashMap<String, String>
	 */
	public MoneyBag(Money money, HashMap<String, String> convert) {
		this.fMoney = money;
		this.fConvert = convert;
	}
	
	/**
	 * Insert new currency convert
	 * @param key String currency
	 * @param value String multiplier
	 */
	public void insert(String key, String value) {
		// No duplicate
		if(!this.convert().containsKey(key)) {
			this.convert().put(key, value);
		}
	}
	
	/**
	 * Add amount from money to current
	 * @param m Money
	 * @return Money
	 */
	public Money add(Money m) {
		String currency = this.money().fCurrency();
		if(m.amount() > 0) {
			// Same currency
			if(m.fCurrency() == currency) {
				return this.money().add(m);
			// Convert
			} else {
				Double multiplier = 1.0;
				if(this.convert().get(m.fCurrency()) != null) {
					multiplier = Double.parseDouble(this.convert().get(m.fCurrency()));
				}
				Double converted = m.amount()*multiplier;
				int amount = converted.intValue();
				
				return this.money().add(amount, currency);
			}
		}
		
		return this.money();
	}
	
	/**
	 * Sub amount from money to current
	 * @param m Money
	 * @return Money
	 */
	public Money sub(Money m) {
		String currency = this.money().fCurrency();
		if(m.amount() > 0) {
			// Same currency
			if(m.fCurrency() == currency) {
				int amount = this.money().amount() - m.amount();
				
				return new Money(amount, currency);
			// Convert
			} else {
				Double multiplier = 1.0;
				if(this.convert().get(m.fCurrency()) != null) {
					multiplier = Double.parseDouble(this.convert().get(m.fCurrency()));
				}
				Double converted = m.amount()*multiplier;
				int amount = converted.intValue();
	
				int namount = this.money().amount() - amount;
				
				return new Money(namount, currency);
			}
		}
		
		return this.money();
	}
	
	/**
	 * Add amount from moneybag to current
	 * @param m MoneyBag
	 * @return MoneyBag
	 */
	public MoneyBag add(MoneyBag m)  {
		Money money = this.add(m.money());
		
		return new MoneyBag(money, this.convert());
	}
	
	/**
	 * Sub amount from moneybag to current
	 * @param m
	 * @return
	 */
	public MoneyBag sub(MoneyBag m) {
		Money money = this.sub(m.money());
		
		return new MoneyBag(money, this.convert());
	}
}
