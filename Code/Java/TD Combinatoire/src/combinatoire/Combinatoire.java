package combinatoire;

import java.util.Vector;

public class Combinatoire {
	
	public Combinatoire() { }

	public static Vector<Combinatoire> unionSet(Vector<Combinatoire> a, Vector<Combinatoire> b) {
		Vector<Combinatoire> vec = new Vector<Combinatoire>();
		
		a.forEach((n) -> vec.add(n));
		b.forEach((n) -> vec.add(n));
		
		return vec;
	}
}
