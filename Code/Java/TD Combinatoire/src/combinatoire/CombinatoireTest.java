package combinatoire;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Vector;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test class
 * @author Aeith
 *
 */
class CombinatoireTest {
	
	private static Vector<Combinatoire> vec;
	
	@BeforeAll
	static void init() {
		vec = new Vector<Combinatoire>();
		vec.add(new Combinatoire());
		vec.add(new Combinatoire());
	}

	@Test
	void testNullParam() {
		Vector<Combinatoire> expected = null;
		Vector<Combinatoire> actual = Combinatoire.unionSet(vec, null);
		
		assertEquals(expected, actual);
	}

	@Test
	void testEmptyParam() {
		Vector<Combinatoire> expected = new Vector<Combinatoire>();
		Vector<Combinatoire> actual = Combinatoire.unionSet(vec, expected);
		
		assertEquals(expected.size(), actual.size());
	}

	@Test
	void testSameVectors() {
		Vector<Combinatoire> a = vec;
		
		Vector<Combinatoire> expected = vec;
		Vector<Combinatoire> actual = Combinatoire.unionSet(a, vec);
		
		assertEquals(expected.size(), actual.size());
	}	
}
